@tenant_level
Feature: check the advances search feature on tenant level
        
        as a tenant admin I should be able to search for users. 
        when a user is found it should be possible to click the user id and group id.
        if the user id is clicked the tenant admin should get redirected to the respective users portal
        and if the group id is clicked the tenant admin should get redirected to the group that contains the respective user

        
        Background: Navigate to the search page
            Given I navigate to the search page

        @firstName
        Scenario: search for users by first name
            Given I provide the first name of one of the available end users
            Then an overview of all the user that have the respective first name should be shown

        @lastName
        Scenario: search for users by last name
            Given I provide the last name of one of the available end users
            Then an overview of all the user that have the respective last name should be shown

        @phoneNumber
        Scenario: search for users by phone number
            Given I provide the phone number of one of the available end users
            Then an overview of all the user that have the respective phone number should be shown
        
        @email
        Scenario: search for users by email
            Given I provide the email of one of the available end users
            Then an overview of all the user that have the respective email should be shown

        @goToEndUser
        Scenario: continue to the correct end user portal
            Given I have searched a user
            When I click the user ID
            Then I should get redirected to the portal of the respective user

        ##NOTE: Searching for groups can be found on the GROUPS page##
        #Scenario: continue to the portal of the correct group portal
            #Given I have searched a group
            #When I click the group ID
            #Then I should get redirected to the portal of the group to which the user I searched for belongs to