@group_level
Feature: Manage hunt groups

    Background: I Navigate to hunt groups
    Given I navigate to the hunt group page
    
    @AddHG
    Scenario Outline: Add a hunt group
    When I click the Add hunt group button
    And I provide all the required information "<huntGroupName>" "<CLI_firstName>" "<CLI_lastsName>" and click Save
    Then A hunt group with the following name "<huntGroupName>" should be added
    
    Examples: Hunt group data
            | huntGroupName  | CLI_firstName | CLI_lastsName |
            | Test HuntGroup | CLI Firstname | cli lastname  |

    @EditHG
    Scenario: Edit a hunt group
    When I click on the relevant hunt group
    When I navigate to the details page and open the hunt group details
    When Edit the fields about the hunt group
    Then A hunt group should be edited

    @DeleteHG
    Scenario: Remove a hunt group
    When I click the trash can symbol next to the hunt group
    And I click "Confirm" in the pop up
    Then The hunt group should be removed

    @DeleteMultipleHG
    Scenario: Remove multiple hunt groups
    When I click the checkboxes next to the hunt groups i want to remove
    And I click the "Delete" button
    And i click "Confirm" in the pop up
    Then the selected hunt groups should be removed