@tenant_level
Feature: Adding a new group to a tenant

        Background:Navigate to the my groups page
            Given I navigate to my groups 

        @addGrp
        Scenario: Login as a tenant admin and successfully add a new group to the tenant       
             Given I click the add group button
             When I provide all the required data
             When I click the save button
             Then A new group should be added to the tenant

        @editGrp
        Scenario: Edit group details as a tenant admin
             Given I select the group I want to edit
             When I click the edit icon, change the group details and save
             # NOTE! GROUP ID SHOULD BE AUTO GENERATED AND NOT EDITABLE
             Then the new group details should saved


        @deleteGrp
        Scenario: Delete a group
             Given I click the delete icon belonging to the group I want to delete
             When I confirm the delete action for groups
             Then the respective group should be deleted


             