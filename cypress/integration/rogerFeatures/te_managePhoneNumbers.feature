@tenant_level
Feature: Managing available phone numbers of a group
#add, edit and remove available phone numbers to or from a group

        @AssignToGroup
        Scenario: Assign a range of phone numbers to a group
            Given I select phone numbers
            When I click the checkboxes of the phone number ranges i want to assign
            When I click the Assign to group button
            When I adjust the slider to the range of phone numbers required by the group
            When I click the next button for phone numbers
            When I select the group I want to assign the phone number range to
            When I click the add button for phone numbers
            Then A phone number range should be assigned to a group
        
        @AddPhoneNumbers
        Scenario: add a range of phone numbers to a group
            Given I select a group I want to assign phone number to
            When I select Phone Numbers
            When I click the Add button
            When I select a range from available ranges
            When I select the starting number and quantity of required numbers
            When I click the check mark button
            When I click the Assign button
            Then A range of phone numbers should be added to the group

        @RemovePhoneNumbers
        Scenario: Remove a range of phone numbers from a group
            Given I navigate to the Phone Numbers page
            When I select the phone number range(s) I want to remove
            When I click the trash can symbol
            Then The phone number ranges should be removed