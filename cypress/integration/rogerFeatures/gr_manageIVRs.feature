@group_level
Feature: managing IVR's on group level
#Add, edit or remove IVR's

        Background: Navigate to call flows
        Given I navigate to the call flows page

        @addIVR
        Scenario: Add an IVR to the group
            Given I click add new IVR
            When I provide the required information to create an IVR
            When I click next
            When I skip the unnecessary steps to create the IVR
            Then An IVR should be created
            
        @editIVR
        Scenario: Edit an existing IVR
            Given I select an IVR from the list to edit
            When I navigate to the Menus page
            When I click the pencil symbol to edit an IVR
            When I edit the fields of the IVR I want to change
            When I click the save button for edit
            Then The IVR should be edited

         @addSubMenu
        Scenario: Add a Submenu to an IVR
            Given I select an IVR from the list for a submenu
            When I navigate to the menu page
            When I click the pencil symbol to add a submenu
            When I switch the action of a key to Go To Submenu
            When I select Add New Submenu under Action data
            When I provide a name for the submenu
            Then A Submenu should be created
        
        @editSubMenu
        Scenario: Edit an existing Submenu
            Given I select an IVR from the list to edit a submenu
            When I navigate to the menus page to edit a submenu
            When I select the Submenus tab
            When I select the Submenu I want to edit
            When I click the pencil symbol
            When I edit the fields I want to change and save it
            Then The selected Submenu should be edited

        @addBusinessHours
        Scenario: Add a business hours schedule to the IVR
            Given I select an IVR from the list to add bunisess hours
            When I navigate to the schedules page
            When I click the add button for bh schedule
            When I click on a specific day and time in the bh schedule to create an entry
            Then A schedule for business hours should be added
        
        @addHolidays
        Scenario: Add a holiday schedule to the IVR
            Given I select an IVR from the list for holidays
            When I navigate to the schedules page for holidays
            When I click the Holiday tab
            When I select a day on the holiday schedule
            When I provide the nessesary information about the event and click save
            Then A holiday schedule should be added to the IVR

        @removeIVR
        Scenario: Remove an IVR from the group
            Given I click the trash can symbol next to the IVR i want to delete
            When I click the Delete button in the pop up screen
            Then The IVR should be removed from the list