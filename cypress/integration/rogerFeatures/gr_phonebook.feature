@group_level
Feature: check phonebook feature on group level
    add, edit and delete contacts

        Background: Navigate to 'phone book' on grouplevel
            Given I navigate to group contacts
        
        @addcontact
        Scenario Outline: add new contacts to the contact list on group level
            Given I click add new group contact
            When I provide "<gr_contactName>" "<gr_phonenr>" as group contact data and click create
            Then "<gr_contactName>" should be added to the group contact list

        Examples: contacts
                  | gr_contactName    | gr_phonenr     |
                  | test contact      | 00212672166637 |
                  | test contact one  | 0032472166638  |
                  | test contact two  | 00212672166637 |
                  | test contact tree | 0032472166638  |




        Scenario Outline: add a new contact with illegal chars in the name fields
            Given I click add new contact
            When I provide "<illegal chars>" in the input fields and click 'save'
            Then a clear error message should be provided and the contact shouldn't get created

        Examples: illegal chars
                  | contact name |
                  | \            |
                  | /            |
                  | #ß↓↑→        |
                  | ♥♠           |


        Scenario: delete all group contacts
            Given I select all the group contacts
            When I click the delete button to delete all group contacts
            And I confirm the delete action of the group contacts
            Then all the group contacts should be deleted