@tenant_level
Feature: manage licenses on tenant level

@navToLicenses
Scenario: navigate to licenses page
    Given I select the group I want to edit the licenses from
    When I navigate to the licenses page
    Then I should be on the group licenses page

@addUsrLicense
Scenario: Add a user license to a group
    Given I click the pensil symbol next to user licenses
    When I select the relevant licenses to add and the amount of user licenses needed
    Then A user license should be added to the group

@addOtherLicense
Scenario: Add an other license to a group
    Given I click the pensil symbol next to other licenses
    When I select the relevant licenses to add and the amount of other licenses needed
    Then An other license should be added to the group

@RemoveUsrLicense
Scenario: Remove a user license from a group
    Given I click the pensil symbol next to user licenses
    When I deactivate the checkbox from the relevant license I want to remove
    Then A user license is removed from the group

@RemoveOtherLicense
Scenario: Remove an other license from a group
    Given I click the pensil symbol next to other licenses
    When I deactivate the checkbox from the relevant license I want to remove
    Then An other license is removed from the group