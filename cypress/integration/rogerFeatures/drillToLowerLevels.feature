@system_level
Feature: Start as a system admin and drill town the lower levels

   As a system admin 
   I want to be able to drill down to the lower levels

        @drillToTenant
        Scenario: Drill from system admin level to tenant level
            Given I am on system level
             When I Click the ID of one of the customers
             Then I should be on the respective tenants portal

        @drillToGroup
        Scenario: Drill from tenant level to group level
            Given I navigate to the groups page
            When I select one of the available groups
            Then I should be on the respective groups portal

        @drillToUser
        Scenario: Drill from group level to end-user level
            Given I navigate to the users page
            When I select one of the users
            Then I should be on the respective end-users portal