@user_level
Feature: manage the announcements on end-user level


        Background: Navigate to the announcements page
            Given I navigate to the announcements page on end-user level

        Scenario: Add a new recorded announcement
            Given I click the 'add' button
            When I select 'record announcement'
            And I start and then end the recording
            Then the recorded annoucments should be added to the annoucements list
