describe('manage contacts on end user level', function () {

    Cypress.config('pageLoadTimeout', 100000)


    before(function () {
       cy.SignInAsEndUser()
    })
    after(()=> {
        cy.clearCookies
    })


    
    it('add new contact', function () {
        cy.get('ul').children().contains('Contacts').click()
        cy.url().should('include', 'contacts')
        cy.contains('Add').click()
        cy.get('input[class="MuiInputBase-input MuiOutlinedInput-input"]').first().type('Contact 1')
        cy.get('input[class="MuiInputBase-input MuiOutlinedInput-input"]').eq(1).type('0472188837')
        cy.contains('Create').click()
        cy.contains('td','Contact 1').should('be.visible')

    })


        
})
    



