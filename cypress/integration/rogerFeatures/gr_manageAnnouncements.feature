@group_level
Feature: Add, edit or remove announcements

    Background: navigate to announcements page
    Given I navigate to the announcements page
    
    @AddAnnouncement
    Scenario: Add an announcement
    Given I click the Add button and select record
    When I make a recording and save it
    Then I should have added an announcement

    @ListenAnnouncement
    Scenario: Listen to an announcement
    Given I click the play symbol
    Then The announcement should play

    @removeAnnouncement
    Scenario: remove an announcement
    Given I click the trash can symbol next to the announcement
    When I select Delete in the pop up
    Then The announcement should be removed