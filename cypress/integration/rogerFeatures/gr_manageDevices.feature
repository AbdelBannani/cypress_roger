@group_level
Feature: managing devices on group level
#add, edit or remove devices

        Background: I Navigate to Devices
        Given I navigate to the Devices page

        @addDevice
        Scenario Outline: Login as a group admin and succesfully add a new device to the group
            Given I click the add devices button
            When I select a device from the supported devices list
            When I provide "<devicename>" and "<MACaddress>" as device management data
            Then "<devicename>" should be added to the device inventory
        
        Examples: deviceData
            | devicename | MACaddress        |
            | Device 1   | C0:04:40:08:AB:94 |
            | Device 2   | C0:04:40:08:AB:95 |
            | Device 3   | C0:04:40:08:AB:93 |
        
        @editDevice
        Scenario Outline: Edit the MAC address of a device
            Given I click on the name of the device I want to edit
            When I click the pensil symbol next to the device
            When I change "<MACaddress>" of the device
            Then The selected device should have a different MAC address

        Examples: MAC adress
            | MACaddress        |
            | C0:04:40:08:AB:92 |
        
        @rebootDevice
        Scenario: Reboot a device
            Given I click on the name of the device i want to reboot
            When I click the Reboot button
            Then The selected device should be rebooted

        @removeDevice
        Scenario Outline: Remove a selected device from the group
            Given I click on "<devicename>"
            When I click the Delete button for devices
            When I click the Delete button in the popup
            Then "<devicename>" should be removed from the group
        
        Examples:
            | devicename|
            | Device 1  |
            | Device 2  |
            | Device 3  | 