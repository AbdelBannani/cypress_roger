import {Given, When, And, Then, Before, After } from 'cypress-cucumber-preprocessor/steps'
import UsersPage from './usersPage'
import LoginPage from '../login/loginPage'
import { GR_PASSWORD, GR_USERNAME } from '../../../fixtures/testData'


Before(() => {
    cy.SignInAsGroupAdmin(GR_USERNAME, GR_PASSWORD)
});


After(() => {
    cy.clearCookies()
    cy.clearLocalStorage()
    cy.window().then((win) => {
        win.sessionStorage.clear()
      })
});


Given ('I navigate to Users', () => {
    UsersPage.navigateToUsersPage()
})

Given ('I click the add user button', () => {
    UsersPage.clickAddUserButton()
})

When ('I provide {string} , {string} and {string} as username', (firstName, lastName, email)=> {
    UsersPage.provideUserDetails(firstName, lastName, email)
})

// And ('I click create')
// And ('I skip the unnecessary steps and save the user')
// Then ('Email" should be added to the users overview')