import LoginPage from '../login/loginPage'


//const ADD_USER_BUTTON = ('button').contains('Add user')
const FIRSTNAME_INPUTFIELD = '[:nth-child(1) > :nth-child(1) > .MuiInputBase-root > .MuiInputBase-input'
const LASTNAME_INPUTFIELD = '[:nth-child(2) > .MuiInputBase-root > .MuiInputBase-input'
const EMAIL_INPUTFIELD = ':nth-child(3) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input'


class UsersPage extends LoginPage
{

    static navigateToUsersPage() {
        cy.contains('Users').should('be.visible').click()

    }

    static clickAddUserButton() {
        cy.get('button').contains('span','Add user').click({ multiple:true, force:true })
    }

    static provideUserDetails(firstName, lastName, email){
        cy.get(FIRSTNAME_INPUTFIELD).type(firstName)
        cy.get(LASTNAME_INPUTFIELD).type(lastName)
        cy.get(EMAIL_INPUTFIELD).type(email)
    }


}

export default UsersPage