Feature: Login as a valid user

  login as a system admin, tenant admin, group admin and as an end-user


Scenario: login as an end user
  Given I open the roger login page
  When I provide the following valid end-user credentials: "abdel.bannani@netaxis.be" "Netaxis123!!"
  And I submit the login form
  Then I should be logged in and the "Incoming calls" link should be visible


Scenario: login as a group admin
  Given I open the roger login page
  When I provide the following valid group admin credentials: "testers_grp_admin" "Netaxis123!!"
  And I submit the login form
  Then I should be logged in and the "Users" link should be visible


Scenario: login as a tenant admin
  Given I open the roger login page
  When I provide the following valid tenant admin credentials: "testers_ten_admin" "Netaxis123!!"
  And I submit the login form
  Then I should be logged in and the "Groups" link should be visible


# Scenario: login as a system admin
#   Given I open the roger login page
#   When I provide the following valid system admin credentials: "ttl_test" "Netaxis123!!"
#   And I submit the login form
#   Then I should be logged in and the "incoming calls" link should be visible
