@group_level
Feature: Manage call queues

        Background: I Navigate to call queues
        Given I navigate to the call queues page

          @AddCQ
          Scenario: add a new call queue
             When I click add a new call queue
             When I provide all the required data and click save
             Then a new call queue should be added

          @WrongData
          Scenario: provide data which is not adhering to stipulations during queue creation process
             Given I click add a new call queue
             When I provide data which is not respecting stipulations and click save
             Then a clear error message should be provided and the queue should not be added

          @EditCQ
          Scenario: edit an existing call queue for call queues
            Given I select a call queue from the list
            When I open the details of an available call queue
            When I edit the call queue details
            Then The details of a call queue should be changed

          @DeleteCQ
          Scenario: delete a call queue
             Given I select the call queue I want to delete
             When I click the delete button
             When I confirm the delete action for call queues
             Then the respective call queue should be deleted