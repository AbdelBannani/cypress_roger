@group_level
Feature: managing users on group level
#add, edit and delete users

    Background: Navigate to Users on grouplevel
        Given I navigate to Users

    @AddNewUser
    Scenario: Login as a group admin and successfully add a new user to the group by providing only the necessary data
        Given I click the add user button
        When I provide "FirstName" , "LastName" and "Email" 
        #And I skip the unnecessary steps and save the user
        #Then "Email" should be added to the users overview



    # @justContinue
    # Scenario Outline: Try to continue without providing the required data
    #     #Given I click the users link
    #     Given I click the add user button
    #     When I provide "<WrongName>" , "<WrongName2>" and "<WrongMail>" as illegal data
    #     Then a user should be created with a first name ?????

    # Examples:
    #     | WrongName     | WrongName2    | WrongMail |
    #     | €∞•–≠         | Dobermann     | abc@xyz   |

    # @EditUser
    # Scenario Outline: Edit an existing user
    #     #Given I click the users link
    #         Given I open the details tab of the user I want to edit
    #         When I edit the users "<FirstName>" , "<LastName>" and "<Email>"
    #         When I edit the users number settings
    #         When I edit the users device  settings
    #         #When I edit the user services
    #         Then the updated users name "<FirstName>" should be changed accordingly
    
    # Examples: changedNames
    #         | FirstName | LastName | Email              | checkMail             |
    #         | Jantje    | Smit     | changed@edited.com | Johndoe@testemail.com |

    # @deleteUser
    # Scenario Outline: Delete user(s)
    #     # Given I navigate back to user page
    #     #  Then I select trash can symbol net to the user I want to delete
    #     #  When I confirm the delete action on user page
    #     #  Then The "<Email>" should be deleted

    #     Given I select the trash can symbol next to the user I want to delete
    #         When I confirm the delete action on user page
    #         Then The "<Email>" should be deleted
    
    # Examples:
    #     | Email              |
    #     | changed@edited.com |
