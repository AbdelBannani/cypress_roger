@group_level
Feature: manage group admins

    Background: navigate to the admins page
    Given I navigate to the admins page

    Scenario: add an admin
    When I click the Add admin button
    And I provide all the relevant admin information and click Create
    Then An admin should be added

    Scenario: Edit an admin
    When I click the pencil symbol next to an admin
    And I edit the fields
    Then An admin should be edited

    Scenario: Remove an admin
    When I click the trash can symbol next to an admin
    And I click confirm in the pop up
    Then An admin should be removed