import { base_url } from '../../../fixtures/testData'

const USERNAME_INPUT = 'input[name="username"]'
const PASSWORD_INPUT = 'input[name="password"]'
const SUBMIT_BTN = 'button[type="submit"]'





class LoginPage{



    static openScp(){
        cy.visit({
            url: base_url,
            failOnStatusCode: false
        })
    };

    static provideEuCreds(username, password){
        cy.get(USERNAME_INPUT).type(username)
        cy.get(PASSWORD_INPUT).type(password)
    }

    static provideGrpCreds(username, password){
        cy.get(USERNAME_INPUT).type(username)
        cy.get(PASSWORD_INPUT).type(password)
    }

    static provideTenCreds(username, password){
        cy.get(USERNAME_INPUT).type(username)
        cy.get(PASSWORD_INPUT).type(password)
    }

    static provideSysCreds(username, password){
        cy.get(USERNAME_INPUT).type(username)
        cy.get(PASSWORD_INPUT).type(password)
    }

    static submitLogin(){
        cy.get(SUBMIT_BTN).click()
    }


    static assertLogedinOnUserLvl(){
        cy.contains('Incoming calls').should('be.visible')
    }

    static assertLogedinOnGroupLvl(){
        cy.contains('Users').should('be.visible')
    }

    static assertLogedinOnTenantLvl(){
        cy.contains('Groups').should('be.visible')
    }




}

export default LoginPage
 
