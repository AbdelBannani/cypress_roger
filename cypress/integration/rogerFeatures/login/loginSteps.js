import {Given, When, And, Before, After } from 'cypress-cucumber-preprocessor/steps'

import LoginPage from '../login/loginPage'
 
After(() => {
    cy.clearCookies()
    cy.clearLocalStorage()
    cy.window().then((win) => {
        win.sessionStorage.clear()
      })
});

Given('I open the roger login page', () =>{
    LoginPage.openScp()
})


//Login on end-user level
When('I provide the following valid end-user credentials: {string} {string}', (username, password) => {
    LoginPage.provideEuCreds(username, password)
})

And('I submit the login form', () =>{
    LoginPage.submitLogin()
})

// Then('I should be logged in on end-user level', () =>{

//     LoginPage.assertLogedinOnUserLvl()
    
// })


//Login on group level

When ('I provide the following valid group admin credentials: {string} {string}', (username, password) =>{
    LoginPage.provideGrpCreds(username, password)
})

// Then('I should be logged in on group level', () =>{

//     LoginPage.assertLogedinOnGroupLvl()
    
// })

// Login on tenant level

When ('I provide the following valid tenant admin credentials: {string} {string}', (username, password) =>{
    LoginPage.provideTenCreds(username, password)
})

// Then('I should be logged in on tenant level', () =>{

//     LoginPage.assertLogedinOnTenantLvl()
    
// })

// Login on system level 
When ('I provide the following valid system admin credentials: {string} {string}', (username, password) =>{
    LoginPage.provideGrpCreds(username, password)
})



// Then('I should be logged in on system level', () =>{
//     //TODO
//     //LoginPage.assertLogedinOnTenantLvl()
    
// })

