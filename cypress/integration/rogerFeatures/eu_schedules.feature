@user_level
Feature: add, edit and delete schedules on end-user level
    Background: navigate to schedules page
    Given I navigate to the schedules page

@addTimeSchedule
Scenario: Add a time schedule
    Given I click the Add time schedule button
    When I provide a name for the time schedule
    Then A time schedule should be added

@addHolidaySchedule
Scenario: Add a holiday schedule
    Given I select the holiday schedule page
    When I click the Add holiday schedule button
    When I provide a name for the holiday schedule and click create
    Then A holiday schedule should be added

@addTimePeriod
Scenario: add a period to a time schedule
    Given I click on the relevant time schedule
    When I click the Add time period button
    When I provide the relevant information about the time period and click the Add button
    Then a period should be added to the time schedule

@addHolidayPeriod
Scenario: add a period to a holiday schedule
    Given I select the holiday tab
    When I click on the relevant holiday schedule
    When I click on the calender where I want to add a period
    When I provide a name for the period and click the Add button
    Then a period should be added to the holiday schedule

@removeTimePeriod
Scenario: Remove a period from a time schedule
    Given I click on the relevant time schedule to delete
    When I click in the relevant period and select Delete
    When I click the Delete button for a period
    Then The selected period should be removed from the time schedule

@removeHolidayPeriod
Scenario: Remove a period from a holiday schedule
    Given I select the holiday tab to delete a period
    When I click on the relevant holiday schedule to delete
    When I click in the relevant holiday period and select Delete
    When I confirm the Delete action for holiday period
    Then The selected period should be removed from the holiday schedule

@removeTimeSchedule
Scenario: Remove a time schedule
    Given I click the trash can symbol next to a time schedule
    When I confirm the delete action for schedules
    Then The time schedule should be removed

@removeHolidaySchedule
Scenario: Remove a holiday schedule
    Given I select the holiday tab to delete a schedule
    When I click the trash can symbol next to a holiday schedule
    When I confirm the delete action for holiday schedules
    Then The holiday schedule should be removed