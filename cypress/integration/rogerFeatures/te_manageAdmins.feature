 @tenant_level
Feature: manage tenant admins

    Background: navigate to the admins page
    Given I navigate to the tenant admins page

    @addAdmin
    Scenario: add an admin
    Given I click the Add admin button
    When I provide all the relevant information and click Create
    Then An admin should be added

    @editAdmin
    Scenario: Edit an admin
    Given I click the pencil symbol next to an admin
    When I edit the tenant admin fields
    Then An admin should be edited

    @deleteAdmin
    Scenario: Remove an admin
    Given I click the trash can symbol next to an admin
    When I click confirm in the pop up
    Then An admin should be removed