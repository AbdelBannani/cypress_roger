@user_level
Feature: device management on end user level

Background: Navigate to devices
    Given I am on the user devices page

@addCell
Scenario: Add a cell phone in mobility settings
    Given I navigate to the mobility tab
    When I click the off net mobility button
    When I click the add button
    When I fill in my cell phone number and save it
    Then A cell phone number should be added

@rmCell
Scenario: Remove a cell phone in mobility settings
    Given I navigate back to the mobility tab
    When I click the off net mobility button again
    When I click the trash can symbol next to my cell phone number
    When I confirm the delete cell phone action
    Then A cell phone number should be removed

@enableFS
Scenario: Enable flexible seat
    Given I navigate to the flexible seat tab
    When I click the use the device of a colleague button
    When I click the assiociate now button
    When I select an available host in the list and save it
    Then An associated host should be configured

@disableFS
Scenario: Disable flexible seat
    Given I navigate back to the flexible seat tab
    When I click the use the device of a colleague button again
    When I deassign the assiociated host
    Then An associated host should be removed