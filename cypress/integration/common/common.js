import {defineStep} from 'cypress-cucumber-preprocessor/steps'
import LoginPage from '../rogerFeatures/login/loginPage'

// defineStep('I open the roger login page', ()  => {
//     LoginPage.openScp()
// })

defineStep('I should be logged in and the {string} link should be visible', link => {
    cy.contains(link).should('be.visible')

})

defineStep('the url should contain the following {string}', url  => {
    cy.url().should('include', url)
})

defineStep('I reload the browser', ()  => {
    cy.reload() 
})



