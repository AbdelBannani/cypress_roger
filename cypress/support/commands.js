import 'cypress-file-upload';
import LoginPage from '../integration/rogerFeatures/login/loginPage';


// Cypress.Commands.add("SignIn", () => {
//     cy.visit('/sign-in/')
//     cy.title().should('eq', 'Conduit')
//     cy.location('protocol').should('eq', 'https:')
//     cy.get('form').within(($form) => {
//         // cy.get() will only search for elements within form, not within the entire document
//         cy.get('input[type = "email"]').type('qamilestone.academy@gmail.com')
//         cy.get('input[type = "password"]').type('admin123')
//         cy.root().submit()   // submits the form yielded from 'within'
//     })
//     cy.contains('Your Feed', { timeout: 10000 }).should('be.visible')
// })




Cypress.Commands.add("SignInAsEndUser", () => {
    cy.clearCookies()
    cy.clearLocalStorage()
    cy.window().then((win) => {
        win.sessionStorage.clear()
      })
    cy.visit({
        url:'/sign-in/',
        failOnStatusCode: false
    })
    cy.location('protocol').should('eq', 'https:') 
    // cy.get() will only search for elements within form, not within the entire document
    cy.get('input[name="username"]').type('abdel.bannani@netaxis.be')
    cy.get('input[name="password"]').type('Netaxis123!!')
    cy.get('button[type="submit"]').contains('Log in').should('be.visible').click()
    cy.get('a[href="/user/abdel.bannani@netaxis.be/recent-calls"]').should('be.visible').click({ multiple:true, force:true })
    
})


Cypress.Commands.add("SignInAsGroupAdmin", (username, password) => {
    cy.clearCookies()
    cy.clearLocalStorage()
    cy.window().then((win) => {
        win.sessionStorage.clear()
      })
    cy.visit({
        url:'/sign-in/',
        failOnStatusCode: false
    })
    cy.location('protocol').should('eq', 'https:') 
    cy.get('input[name="username"]').type(username)
    cy.get('input[name="password"]').type(password)
    cy.get('button[type="submit"]').contains('Log in').should('be.visible').click()
    cy.get('a[href="/group/ENT4TESTERS_g4297/users"]').should('be.visible')//.click({ multiple:true, force:true })

})

Cypress.Commands.add("SignInAsTenantAdmin", () => {
    cy.clearCookies()
    cy.clearLocalStorage()
    cy.window().then((win) => {
        win.sessionStorage.clear()
      })
    cy.visit({
        url:'/sign-in/',
        failOnStatusCode: false
    })
    cy.location('protocol').should('eq', 'https:') 
    cy.get('input[name="username"]').type('testers_ten_admin')
    cy.get('input[name="password"]').type('Netaxis123!!')
    cy.get('button[type="submit"]').contains('Log in').should('be.visible').click()
    cy.get('a[href="/tenant/groups"]').should('be.visible').click({ multiple:true, force:true })

})

Cypress.Commands.add("SignInAsSystemAdmin", () => {
    cy.clearCookies()
    cy.clearLocalStorage()
    cy.window().then((win) => {
        win.sessionStorage.clear()
      })
    cy.visit({
        url:'/sign-in/',
        failOnStatusCode: false
    })
    cy.location('protocol').should('eq', 'https:') 
    cy.get('input[name="username"]').type('abdel.bannani@netaxis.be')
    cy.get('input[name="password"]').type('Netaxis123!!')
    cy.get('button[type="submit"]').contains('Log in').should('be.visible').click()
    cy.get('a[href=/user/abdel.bannani@netaxis.be/recent-calls"]').should('be.visible').click({ multiple:true, force:true })
    
})